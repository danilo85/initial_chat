module.exports = function(app) {
	var Client = require('node-rest-client').Client 
	,Crypto = app.config.authentication
	,Message = app.models.message
	,Habilidades = app.models.habilidades
	,Usuarios = app.models.usuarios
	,Objetivos = app.models.objetivos
	,config = app.settings.config
	,commun = app.lib.communs
	,mongoose = app.models.factore.mongoose; 

	let response = {
		status  : 200,
		success : 'Updated Successfully'
	};

	let controller = {
		index : function(req, res){
			let index_callback = function(data, response){
				let date = new Date().getTime().toString();
				app.io.privateRoom = Crypto.encrypt(date, config.pwd);

				let names = Crypto.encrypt(JSON.stringify({data: date, name: 'idSocket'}), config.pwd);				
				let apache = '/apache/index.php';
				let index_parameter = {
					id:  app.io.privateRoom,
					url: config.views + '/', 
					iframe: config.views + apache,
					path: config.path,
					server: config.server,
					string: names,
					version: config.version,
					key : data.token
				};

				if (req.query.page && req.query.post)
					index_parameter.iframe = config.views + apache + '?page=' + req.query.page + '&post=' + req.query.post;

				if (config.secure && config.views === 'https://www.yggjobs.com'){
					if (req.params.id){
						for (var i = 0; i < config.permissoes.length; i++) {
							if (config.permissoes[i] === req.params.id){
								res.render('index', index_parameter);
								return;
							}
						}
					}
					res.redirect('https://www.yggboard.com');
					return;
				}

				res.render('index', index_parameter);
				return;
			}

			let client = new Client();
			let url = config.server + 'rest/yggboard_server/rest/usuario/login?email=marly1964@gmail.com&password=danilo&tokenadm=true';
			client.get(url, index_callback);
			return;
		},

		createRoom : function(req, res){
			if (req.query.a && req.query.b){
				let room = Crypto.encrypt(req.query.a+req.query.b, config.pwd);
				let create = new Message();
				create.documento.room = room;
				create.documento.messages = [];
				create.save();
				res.json({token :  room});
				res.end(JSON.stringify(response));
				return;
			}
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		login : function(req, res){
			var room = req.body.room, json = req.body.json;
			if (room && json){
				app.io.in(room).emit('login', json);
				res.json({status :  'ok'});
				res.end(JSON.stringify(response));
				return;
			}
			
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		logout : function(req, res){
			var room = req.query.room;
			if (room){
				app.io.in(room).emit('logout', room);
				res.json({status :  'ok'});
				res.end(JSON.stringify(response));
				return;
			}
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		refresh : function(req, res){
			var room = req.body.room, json = req.body.json;
			if (room && json){
				app.io.in(room).emit('refresh', json);
				res.json({status: 'ok'});
				res.end(JSON.stringify(response));
				return;
			}
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		destroy : function(req, res){
			var room = req.body.room, json = req.body.json, event = req.body.event;
			if (room && json){
				Message.findOneAndRemove({'documento.room': room}, function(err, message){
					if (err){
						res.statusCode = 401;
						res.send('bad resquest');
						return;
					}
				});
				app.io.in(event).emit('refresh', json);
				res.json({status: 'ok'});
				res.end(JSON.stringify(response));
				return;
			}
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		feed : function(req, res){
			let body = req.body;
			if (body.id_usuario){
				app.io.in(body.id_usuario).emit('feed', body);
				res.send({feed: body});
				res.end(JSON.stringify(response));
				return;
			}
			res.statusCode = 401;
			res.send('bad resquest');
			return;
		},

		statistic : function(req, res){
			let server = 'https://www.yggboard.com/';
			let statistic_callback = function(data, response){
				if (req.params.id){
					for (var i = 0; i < config.permissoes.length; i++) {
						if (config.permissoes[i] === req.params.id){
							res.render('statistic', {
								path: config.path,
								url: config.views + '/', 
								version: config.version,
								server : server,
								key : data.token
							});
							return;
						}
					}
				}

				res.redirect('https://www.yggboard.com');
				return;
			};

			let client = new Client();
			let url = server + 'rest/yggboard_server/rest/usuario/login?email=marly1964@gmail.com&password=danilo&tokenadm=true';
			client.get(url, statistic_callback);
			return;
		},
		
		usuarios : function(req, res){
			let id = req.query.id;
			res.json({dateTimeAcceptTerms : mongoose.Types.ObjectId(id).getTimestamp()});
			res.end(JSON.stringify(response));			
			return;
		},
		xlsx : function(req, res){
			let body = req.body;
			var jsonArr = [{
				foo: 'bar',
				qux: 'moo',
				poo: 123,
				stux: new Date()
			},{
				foo: 'bar',
				qux: 'moo',
				poo: 345,
				stux: new Date()
			}];
			console.log(jsonArr);
			res.xls('data.xlsx', jsonArr);
		}
	};


	return controller;
};