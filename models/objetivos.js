module.exports = function(app) {
	let mongoose = app.models.factore.mongoose; 
	let connection = app.models.factore.homologacao;

	let schema = mongoose.Schema({
		documento : {
			id : String,
			nome : String,
			nivel : String,
			nivelFiltro : String,
			segmentoEconomico : String,
			areaAtuacao : [],
			areaAtuacaoNome : [],
			responsabilidades : String,
			atividades : String,
			salarioMinimo : String,
			salarioMaximo : String,
			salarioMedio : String,
			tags : [],
			necessarios : [],
			necessariosNome : [],
			preRequisitosGeral : [],
			recomendados : [],
			recomendadosNome : [],
			classificacao : String
		}
	},
	{versionKey: false});

	return connection.model('objetivos', schema);
};