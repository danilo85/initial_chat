module.exports = function(app) {
  var controller = app.controllers.controller;
  
  app.get('/users', controller.createRoom);  
  app.get('/logout', controller.logout);

  app.post('/refresh', controller.refresh);
  app.post('/login', controller.login);  
  app.post('/feed', controller.feed);
  app.post('/destroy', controller.destroy);
 
  app.get('/', controller.index);
  app.get('/:id', controller.index);
  app.get('/:id/statistic', controller.statistic);
  app.get('/:id/statistic/usuarios', controller.usuarios);
  app.get('/:id/statistic/usuarios/xlsx', controller.xlsx);
};
