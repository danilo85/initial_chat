var express = require('express')
, load = require('express-load')
, app = express()
, server = require('http').createServer(app)
, https = require('https')
, port = 80
, sport = 443
, io = require('socket.io')(server)
, fs = require('fs')
, forceSSL = require('express-force-ssl')
, forceDomain = require('express-force-domain')
, proxy = require('http-proxy').createProxyServer({})
, cors = require('cors')
, bodyParser = require('body-parser')
, jsonConfig = {
	"apache": "http://localhost:3000/",
	"rest" : "https://www.yggjobs.com/rest",
	"django" : "https://www.yggjobs.com/dj",
	"views" : "http://localhost",
	"path" : "rest/yggboard_server/rest/",
	"server" : "https://www.yggjobs.com/",
	"secure" : false,
	"permissoes": ["allan","arthur","danilo","douglas","eric","grenne"],
	"version": "1.0.7",
	"pwd": "ygglicious13"
}
, path = 'd:/node/config.json',
json2xls = require('json2xls');;

if (fs.existsSync(path)){
	var jsonConfig = JSON.parse(fs.readFileSync(path).toString());
	var options = {
		key: fs.readFileSync('d:/ssl/ssl.key/'+jsonConfig.ssl.key+'.pem'),
		cert: fs.readFileSync('d:/ssl/ssl.crt/'+jsonConfig.ssl.crt+'.pem'),
		ca : fs.readFileSync('d:/ssl/ssl.crt/gd_bundle-g2-g1.pem')
	};

	var secureServer = https.createServer(options, app).listen(sport, function(){
		console.log('Listening on port ' + secureServer.address().port);
	});

	app.use(cors({origin: 'http://localhost', credentials: true}));
	io = require('socket.io')(secureServer);
	app.use(forceSSL);
	app.use(forceDomain(jsonConfig.views));
}

var listener = server.listen(port, function(){
	console.log('Listening on port ' + listener.address().port);
});

app.use('/apache/', function(req, res) {
	proxy.web(req, res, {
		target: jsonConfig.apache
	});
});

app.use('/rest/', function(req, res) {
	proxy.web(req, res, {
		target: jsonConfig.rest
	});
});

app.use('/dj/', function(req, res) {
	proxy.web(req, res, {
		target: jsonConfig.django
	});
});

app.use(express.static(__dirname + '/public'));
app.use(json2xls.middleware);
app.set('view engine', 'ejs');
app.set('views','./views');
app.set('config', {
	views: jsonConfig.views,
	path: jsonConfig.path,
	server: jsonConfig.server,
	secure: jsonConfig.secure,
	version: jsonConfig.version,
	permissoes: jsonConfig.permissoes,
	pwd: jsonConfig.pwd
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.io = io;

load('config').then('models').then('lib').into(app)
load('sockets').into(app);
load('controllers').then('routes').into(app);

module.exports = app;