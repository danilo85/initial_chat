var FADE_TIME = 150;
var TYPING_TIMER_LENGTH = 400;
var COLORS = ['rgba(137,83,179,0.5)', 'rgba(116,188,86,0.5)', 'rgba(0,168,177,0.5)', 'rgba(252,109,103,0.5)', 'rgba(0,22,155,0.5)', 'rgba(0,0,0,0.2)'];
var $usernameInput = $('.usernameInput');
var $passwordInput = $('.passwordInput');
var $btnInput = $('.btnInput');
var $loginPage = $('.login.page');
var $usersPage = $('.users.page');
var $userPerfil = $('.userPerfil');
var $chatMinimize = $('.chatMinimize');
var $chatClose = $('.chatClose');
var body,$notify_message = undefined;
var username, password;
var login = undefined;
var typing = false;
var socket = io({
 reconnection: true,
 reconnectionDelay: 1000,
 reconnectionDelayMax : 5000,
 reconnectionAttempts: Infinity
});

var rooms = []; 
var follows = [];

function getMt(d){
  var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  return month[d];
}

function checkDay(current_day, message_day){
  if (current_day == message_day){
    return 'Hoje às ';
  }else if(current_day-1 == message_day){
    return 'Ontem às ';
  }
  return false;
}

var usuarios = function(data, var1, var2, var3){
  if(data){
    if(data.documento){
      var li = createElements('li', 'userPerfil');
      var img = data.documento.photo ? '<img style="width: 60px;" src="'+getImagem(data.documento.photo, 'perfil') +'">' : '<img style="width:60px;" src="apache/dashboard/img/badgeex.png">';
      li.innerHTML = img + data.documento.firstName;
      li.setAttribute('data-email', var2);
      li.setAttribute('data-room', var1);
      li.setAttribute('data-name', data.documento.firstName);
      $(li).addClass('online');
      var3.appendChild(li);
    }
  }
}

var userPerfil = function(data, var1, var2, var3){
  if(data){
    if (data.documento.seguindo){
      var seguindo = data.documento.seguindo;
      for (var i = 0; i < seguindo.length; i++) {
        follows.push(seguindo[i]);
      }
    }
  }
}

function addUser(user){
  socket.emit('set user in room', user);
  var $el = $('span[data-email="'+user.emails[0]+'"');
  $el.animate({ opacity: 0.25, left: "+=50", height: "toggle" }, 5000, function() { $($el).remove(); });
}

function log(message, options, data) {
  var $el = $('<li>').addClass('log').text(message);
  addMessageElement($el, options, data);
}

function main(data){
  if (data) {    
    login = data;
    var conexoes = login.conexoes;
    if (conexoes){
      if (conexoes.length != 0){
        var ul = createElements('ul');
        for (var i = 0; i < conexoes.length; i++) {
          obterPerfil("usuarios", "_id", usuarios, conexoes[i].user_id, undefined, conexoes[i].room, conexoes[i].user_id, ul);
          rooms.push(conexoes[i].room);
        }
        $usersPage.html('').append(ul);
        $('.panel.panel-chat').show();
        $chatMinimize.click();
      }

      obterPerfil("userPerfil", "_id", userPerfil, login.idUserPerfil);
      follows.push(login._id);

      username = login.firstName;
      socket.token = login.token;
      
      socket.emit('new room', {
        room: rooms,
        follows: follows,
        name: username, 
        email: login._id
      });  
    }  
    return;
  }else{
    console.log('falta login');
    return;
  }
}

function sendMessage(data) {
  if (login){
    addChatMessage(data, undefined, 'Me');
    socket.emit('message', data);
  }
}

function createRoom(data){
  
  var $divroom = $('div[data-room="'+data.room+'"]');
  if ($divroom.length > 0)
    return false;

  var chats_length = $('.panel.panel-chat').length;
  var multi = (chats_length)*315;
  var email = data.emails.indexOf(login._id) == 0 ? 1 : 0;
  var panel_chat = createElements('div', 'panel panel-chat');
  panel_chat.setAttribute('style', 'background-color: #FFF;right:'+multi+'px;');
  panel_chat.setAttribute('data-posicao', chats_length);

  var panel_heading = createElements('div', 'panel-heading');

  var span = createElements('span', 'spanNome');
  span.innerHTML = data.names[email];
  panel_heading.appendChild(span);

  var a = createElements('a', 'chatClose');
  a.innerHTML = 'X';
  a.href = 'javascript:void(0);';
  a.setAttribute('data-room', data.room);
  panel_heading.appendChild(a);

  $(a).click(function(){
    close($(this));
  });

  var a = createElements('a', 'chatMinimize');
  a.innerHTML = '_';
  a.href = 'javascript:void(0);';
  a.setAttribute('data-room', data.room);
  panel_heading.appendChild(a);

  $(a).click(function(){
    minimize(this);
  });

  var clearFix = createElements('div','clearFix');
  panel_heading.appendChild(clearFix);
  
  panel_chat.appendChild(panel_heading);

  var panel_body = createElements('div','panel-body messages');
  panel_body.setAttribute('data-room', data.room);
  panel_body.setAttribute('data-status', false);
  panel_chat.appendChild(panel_body);

  var panel_footer = createElements('div','panel-footer');
  panel_footer.setAttribute('data-room', data.room);

  var input = createElements('input','inputMessage');
  input.setAttribute('placeholder', 'Escreva aqui...');
  input.setAttribute('data-room', data.room);
  input.setAttribute('data-name', username);
  input.setAttribute('data-talkTo', data.emails[email]);
  panel_footer.appendChild(input);
  

  panel_chat.appendChild(panel_footer);

  $('div[data-page="chat"]').append(panel_chat);

  if (data.message && data.message.length > 0){
    var message = data.message;
    for (var i = 0; i < message.length; i++) {
      var classe = message[i].email === login._id ? 'Me' : 'Her';
      addChatMessage(message[i], undefined, classe);
    }
  }
}

function addChatTyping (data) {
  data.typing = true;
  data.message = 'Escrevendo...';
  addChatMessage(data, undefined, 'Her');
}

function updateTyping (data) {
  if (login) {
   if (!typing) {
    typing = true;
    socket.emit('typing', data);
  }
  lastTypingTime = (new Date()).getTime();

  setTimeout(function () {
    var typingTimer = (new Date()).getTime();
    var timeDiff = typingTimer - lastTypingTime;
    if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
     socket.emit('stop typing', data);
     typing = false;
   }
 }, TYPING_TIMER_LENGTH);
}
}

function addChatMessage(data, options, classe) {
  var $typingMessages = getTypingMessages(data);
  options = options || {};
  if ($typingMessages.length !== 0) {
   options.fade = false;
   $typingMessages.remove();
 }

 var divMessage = createElements('div', 'message message'+classe);
 divMessage.style.backgroundColor = getUsernameColor(data.name);

 var span = createElements('span' , 'messageBody');
 var spanMsg = createElements('span' , 'msgSpan');
 spanMsg.innerHTML = data.message;
 span.append(spanMsg);

 if (data.current_time){
   var message_time = new Date(data.current_time);
   var current_time = new Date().getDate();
   var time = message_time.getHours() +':'+ message_time.getMinutes(); 
   var dia = checkDay(current_time, message_time.getDate());
   var descricao_data =  dia ? dia + time : message_time.getFullYear() +' '+ getMt(message_time.getMonth())  +' '+ message_time.getDate()  +', '+ time;

   var footer = createElements('footer');
   footer.style.color = '#868e96';
   footer.style.fontSize = 'smaller';
   footer.innerHTML = descricao_data;
   span.appendChild(footer);
 }

 var typingClass = data.typing ? 'typing' : '';
 var $messageDiv = $(divMessage).data('username', data.name).addClass(typingClass).append(span, '<div class="clearFix">');
 addMessageElement($messageDiv, options, data);
}

function removeChatTyping (data) {
  getTypingMessages(data).fadeOut(function () {
   $(this).remove();
 });
}

function getTypingMessages (data) {
  return $('.typing.message').filter(function (i) {
   return $(this).data('username') === data.name;
 });
}

function getUsernameColor (username) {
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
   hash = username.charCodeAt(i) + (hash << 5) - hash;
 }

 var index = Math.abs(hash % COLORS.length);
 return COLORS[index];
}

function addMessageElement(el, options, data) {
  var $el = $(el);
  var $messages = $('.messages[data-room="'+data.room+'"]');
  var status = $messages.attr('data-status') === 'true';

  if ($messages.length == 0){
    if (data.typing)
      return;

    var $painel = $('.panel-body');
    var status = $painel.attr('data-status') === 'true';
    var $notifications = $('span[data-email="'+data.email+'"]');
    var text = $notifications.length > 0 ? parseInt($notifications.text())+1 : 0;

    if (text == 0){
      $('li[data-email="'+data.email+'"]').append(returnBadgeSpan(data.email, 1));
      if (status)
        $painel.parent().effect('shake');

      return;
    }

    $('span[data-email="'+data.email+'"]').html('').html(text).effect('highlight');

    if (status)
      $painel.parent().effect('shake');

    return;
  }

  if (status && !data.typing)
    $messages.parent().effect('shake');

  if (!options)
   options = {};

 if (typeof options.fade === 'undefined')
   options.fade = true;    

 if (typeof options.prepend === 'undefined')
   options.prepend = false;

 if (options.fade) 
   $el.hide().fadeIn(FADE_TIME);

 if (options.prepend) 
   $messages.prepend($el);
 else
   $messages.append($el);

 $messages[0].scrollTop = $messages[0].scrollHeight;
}

function minimize(element){
  var $room = $(element).data('room');
  
  var roomElement = !$room ? '[data-room="first"]' : '[data-room="'+$room+'"]';
  $('.panel-body'+roomElement+', .panel-footer'+roomElement).slideToggle("slow", function(){
    if ($(this).css('display') === 'none'){
      $(this).attr('data-status', true);
      $(element).addClass('inverte');
    }else{
      $(this).attr('data-status', false);
      $(element).removeClass('inverte');
    }
  });
}

function close($element){  
  var element = $element.parentsUntil()[1];  
  var posicao_elementos = $(element).attr('data-posicao');

  $('div[data-page="chat"]').children().each(function(){
    posicoes = $(this).attr('data-posicao');
    if (posicoes){
      if (posicao_elementos != posicoes && posicao_elementos < posicoes){
        inicio = parseInt($(this).css("right"));
        fim = inicio-315;
        $(this).css("right", fim+'px');
        $(this).attr('data-posicao', posicoes-1);
      }
    }
  });

  element.remove();
  $('li[data-room="'+$element.attr('data-room')+'"]').removeClass('talk').addClass('online');
}

function returnBadgeSpan(email, length){
  return '<span data-email="'+email+'" class="badge badge-pill badge-success" style="background-color: #5cb85c;">'+length+'</span>'
}

$(document).on('keydown', '.inputMessage', function(event){
  if (event.which === 13 && this.value != ''){
   var message = {current_time: new Date(), email: login._id, room: this.getAttribute('data-room'), message: this.value, name: this.getAttribute('data-name')};
   sendMessage(message);
   socket.emit('stop typing', message);
   this.value = '';
   typing = false;
 }
});

$(document).on('input', '.inputMessage', function() {
  updateTyping({room: this.getAttribute('data-room'), name: this.getAttribute('data-name')})
});

$chatMinimize.click(function(){
  minimize(this);
});

$(document).on('click', '.userPerfil.online', function(){
  var user = {
   names : [this.getAttribute('data-name'), username],
   emails : [this.getAttribute('data-email'), login._id],
   room: this.getAttribute('data-room'),
   from : this.getAttribute('data-email')
 }
 addUser(user);
});

socket.on('user joined', function (data) {
  for (var i = 0; i < data.emails.length; i++) {
   $('li[data-email="'+data.emails[i]+'"].online').removeClass('online').addClass('talk');
 }
 createRoom(data);
 $('input[data-room="'+data.room+'"]').focus();
});

socket.on('new user', function (data) {
  if (login){
   $('.userPerfil').each(function(){
    var $elemento = $(this);
    if (data.email === $elemento.data('email')){
     $elemento.find('img').css('border','2px solid green');
     socket.emit('imHere', login._id);
   }
 });
 }
});

socket.on('iKnow', function(data){
  if (login)
    $('li[data-email="'+data+'"].online').find('img').css('border','2px solid green');
});

socket.on('message', function (data) {
  addChatMessage(data, undefined, 'Her');
  if ($('.messages[data-room="'+data.room+'"]').length > 0){
    socket.emit('read', data);
  }
});

socket.on('typing', function (data) {
  addChatTyping(data);
});

socket.on('stop typing', function (data) {
  removeChatTyping(data);
});

socket.on('user left', function (data) {
  if (login && data){
    $('li[data-email="'+data.email+'"]').find('img').css('border','none');
  }
});

socket.on('disconnect', function () {
  console.log('disconnect');
});

socket.on('feed', function (data) {
  console.log('feed: ', data);
  body = document.getElementById("plataforma").contentWindow.document.body.childNodes;
  menu_fechado = $(body).find('#navLateral');
  menu_aberto = $(body).find('#menu');

  $(menu_fechado).find('a[data-event="notify"]').css('background-color','red');
  $(menu_aberto).find('a[data-event="notify"]').css('background-color','red');

  feedholder = $(body).find('#feedholder');
  prepend = $(feedholder).find('.clearfix.last').parent();

  $(prepend).find('li').removeClass('last');
  $(prepend).prepend(addFeed(data, 'last'));
});

socket.on('login', function (data) {
  if ($('.panel.panel-chat').css('display') === 'none') {
    body = document.getElementById("plataforma").contentWindow.document.body.childNodes;
    menu_fechado = $(body).find('#navLateral');
    menu_aberto = $(body).find('#menu');
    
    //$notify_message = $(menu_fechado).find('a[data-event="message"]');
    //$notify_message = $(menu_aberto).find('a[data-event="message"]');
    main(getJson(data));
  }  
});

socket.on('logout', function (data) {
  localStorage.clear();
  location.reload(); 
});

socket.on('notifications', function (data) {
  var $li = $('li');
  for (var i = 0; i < data.length; i++) {
    $('li[data-email="'+data[i].email+'"]').append(returnBadgeSpan(data[i].email, data[i].message));
  }
});

socket.on('refresh', function (data) {
  var dataa = getJson(data);
  if (dataa.conexoes){
    if (dataa.conexoes.length != 0 ){
      main(dataa);
      return;
    }
  }
  
  $usersPage.html('');
  $('.panel.panel-chat').hide();
});