module.exports = function(app) {
    let mongoose = app.models.factore.mongoose; 
    let connection = app.models.factore.homologacao;

    let schema = mongoose.Schema({
        documento : {
            id : String,
            area : String,
            campo : String,
            categoria : String,
            nome : String,
            descricao : String,
            areaConhecimento : [],
            areaConhecimentoNome : [],
            preRequisitos : [],
            preRequisitosNome : [],
            preRequisitosGeral : [],
            tags : [],
            wiki : String,
            amazon : String,
            video : String,
            _id : String,
            objetivos : [],
            objetivosNome : [],
            cursos : [],
            cursosNome : []
        }
    },
    {versionKey: false});

    return connection.model('habilidades', schema);
};