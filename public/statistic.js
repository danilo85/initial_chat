// var1 = variavel de retorno;
// var2 = email
// var3 = userPerfil (colletion)
// data = usuarios (colletion)

// https://www.yggjobs.com/rest/yggboard_server/rest/carreiras/lista
// https://www.yggjobs.com/rest/yggboard_server/rest/habilidades/lista
// https://www.yggjobs.com/rest/yggboard_server/rest/cursos/lista
// https://www.yggjobs.com/rest/yggboard_server/rest/cursos/lista
// https://www.yggjobs.com/rest/yggboard_server/rest/badges/lista


// ObjectId("59f0d0430487f01c941529e9").getTimestamp()

class Statistic {

    constructor() {
        this.url = server + restpath + '/userPerfil/lista';
        this.json = {
            usuarios : [], 
            percetage_total_user : 0,
            media_habilidades_user: 0
        };
    }

    getRelatorio(returnn, volta){
        let json = undefined;
        var checkPreenchidos = function(vetor, esperado){
            let preenchidos = 0;

            for (var i = 0; i < vetor.length; i++) {
                var linha = vetor[i];
                for (var w in linha){
                    if (linha[w] != '')
                        preenchidos++;
                }
            }
            return preenchidos;
        };

        var retorno = function (data, var1, var2, var3){

            let json = {};
            let usuarios = data.documento;
            let porcentagem_perfil = 0;
            let somatorio = 0;
            if (usuarios.objetivos && usuarios.interesses && usuarios.cursos && usuarios.empresas){
                if (usuarios.objetivos.length != 0 && usuarios.interesses.length != 0 && usuarios.cursos.length != 0 && usuarios.empresas.length != 0){
                    let preenchidos_objetivos = checkPreenchidos(usuarios.objetivos);
                    let esperado_objetivos = usuarios.objetivos.length*3;
                    let percetage_objetivos = preenchidos_objetivos > esperado_objetivos ? 100 : parseInt((preenchidos_objetivos*100)/esperado_objetivos);
                    let objetivos = percetage_objetivos;

                    let interesses = usuarios.interesses.length > 0 ? 100 : 0;

                    let preenchidos_cursos = checkPreenchidos(usuarios.cursos);
                    let esperado_cursos = usuarios.cursos.length*3;
                    let percetage_cursos = preenchidos_cursos > esperado_cursos ? 100 : parseInt((preenchidos_cursos*100)/esperado_cursos);
                    let cursos = percetage_cursos;

                    let preenchidos_empresas = checkPreenchidos(usuarios.empresas);
                    let esperado_empresas = usuarios.empresas.length*6;
                    let percetage_empresas = preenchidos_empresas > esperado_empresas ? 100 : parseInt((preenchidos_empresas*100)/esperado_empresas);
                    let empresas = percetage_empresas;

                    let total = (objetivos+interesses+cursos+empresas);
                    
                    let percetage = 100;
                    if (total != 400)
                        percetage = parseInt((total*100)/400);

                    porcentagem_perfil = percetage;
                    var1.percetage_total_user += (preenchidos_objetivos + preenchidos_cursos + preenchidos_empresas + usuarios.interesses.length);
                }
            }
            
            json['nome'] = usuarios.firstName + ' ' + usuarios.lastName;
            let dateTimeAcceptTerms = rest('danilo/statistic/usuarios?id='+data._id, false ,'GET', callback, false, getUrl()).dateTimeAcceptTerms;
            json['data_cadastro'] = usuarios.dateTimeAcceptTerms ? format_Date(new Date(usuarios.dateTimeAcceptTerms), true) : format_Date(new Date(dateTimeAcceptTerms), true);
            json['porcentagem_perfil'] = porcentagem_perfil + '%';
            json['quantidade_login_mes'] = '';
            json['duracao_login'] = '';

            let habilidades = var3.habilidades ? var3.habilidades.length : 0;
            var1.media_habilidades_user += habilidades;
            let habilidadesInteresse = var3.habilidadesInteresse ? var3.habilidadesInteresse.length : 0;
            json['quantidade_habilidade'] = habilidades +'/'+ habilidadesInteresse;
            json['carreiras_interesse'] = var3.carreirasInteresse ? var3.carreirasInteresse.length : 0;
            json['cursos_interesse'] = var3.cursosInteresse ? var3.cursosInteresse.length : 0;
            json['quantidade_post'] = '';
            json['quantidade_seguindo'] = var3.seguindo ? var3.seguindo.length : 0;
            json['utilizcao_chat'] = usuarios.conexoes.length != 0;
            var1.usuarios.push(json);
            document.getElementsByTagName('abbr')[0].innerHTML = var2;
            return json;
        };
        console.log('Processando...');
        $.ajax({
            url: this.url,
            contentType : 'application/json; charset=utf-8',
            dataType : 'json',
            async : true,
            success : function(data) {
                console.log('Lendo usuarios...', data.length);
                for (let i = 0; i < data.length; i++) {
                    console.log(data[i].documento.usuario);
                    obterPerfil('usuarios', 'documento.email', retorno, data[i].documento.usuario, k, returnn, data[i].documento.usuario, data[i].documento);
                }
                volta(returnn);
            }
        });
    }
}

let Statisticc = new Statistic();

let asyncFunction = function(objeto){
    let total = objeto.percetage_total_user;
    let user = objeto.usuarios.length;
    let percetage_total = parseInt((total/(user*12))*100);
    objeto.percetage_total_user = percetage_total + '%';
    objeto.total_user = user;
    objeto.media_habilidades_user = (objeto.media_habilidades_user/user);
    console.log(($('btnGerar'));
    console.log('Fim:', objeto);
};

Statisticc.getRelatorio(Statisticc.json, asyncFunction);
