module.exports = function(app) {

	var io = app.io
	, onlines = []
	, Message = app.models.message
	, Crypto = app.config.authentication;

	io.on('connection', function (socket) {

		onlines.push(socket.id);
		socket.join(socket.id);
		socket.join(app.io.privateRoom);

		socket.on('new room', function (data) {
			socket.join('admin');

			for (var i = 0; i < data.room.length; i++) {
				socket.join(data.room[i]);
			}
			
			for (var i = 0; i < data.follows.length; i++) {
				socket.join(data.follows[i]);
			}

			var returnArrayMessage = [];

			Message.find({'documento.room': {$in : data.room}}, function (err, message) {
				if (message && message.length > 0) {
					var date_message = message;

					for (var i = 0; i < date_message.length; i++) {
						var documento = date_message[i].documento;
						var room = documento.room;
						var messages = documento.messages;

						var message = [];
						var email, current_time, room;
						for (var j = 0; j < messages.length; j++) {
							if (!messages[j].read && messages[j].who.user.email != data.email){
								message.push(Crypto.decrypt(messages[j].who.message, room));
								email = messages[j].who.user.email;
							}
						}
						if (message.length > 0){
							returnArrayMessage.push({
								email : email,
								message : message.length
							});
						}
					}
				}
				if (returnArrayMessage.length > 0)
					socket.emit('notifications', returnArrayMessage);

			});

			socket.data = data;
			socket.broadcast.emit('new user', data);
		});

		socket.on('imHere', function(data){
			socket.broadcast.emit('iKnow', data);
		});

		socket.on('set user in room', function (data) {
			socket.join(data.room);

			Message.findOne({'documento.room': data.room}, function (err, message) {
				if (err) {
					console.log('erro');
				} else {
					if (message){
						let update = message;
						let messages = update.documento.messages;
						for (var i = 0; i < messages.length; i++) {
							if (messages[i].who.user.email === data.emails[0]){
								messages[i].read = true;
							}
						}
						update.save();
					}
				}
			});

			Message.find({'documento.room': data.room}, function (err, message) {
				if (message && message.length > 0) {
					var arrayMessage = message[0].documento.messages
					var returnArrayMessage = [];
					for (var i = 0; i < arrayMessage.length; i++) {
						returnArrayMessage.push({
							name : arrayMessage[i].who.user.name,
							email : arrayMessage[i].who.user.email,
							message : Crypto.decrypt(arrayMessage[i].who.message, data.room),
							current_time : arrayMessage[i].current_time,
							room : data.room
						});
					}
					data.message = returnArrayMessage;
				}
				io.to(data.room).emit('user joined', data);
			});
		});

		socket.on('message', function (data) {
			Message.findOne({'documento.room': data.room}, function (err, message) {
				if (err) {
					console.log('erro');
				} else {
					let update = message;
					let array_update = message.documento.messages;
					array_update.push({
						who : {
							user: {
								name: data.name,
								email: data.email
							}, 
							message: Crypto.encrypt(data.message, data.room)
						},
						read : false,
						current_time: data.current_time
					});
					update.messages = array_update;

					update.save(function (err, allMessage) {
						if (err) 
							console.error(err);

						var lengthh = allMessage.documento.messages.length-1;
						data.id_message = allMessage.documento.messages[lengthh]._id;
						socket.to(data.room).emit('message', data);
					});
				}    
			});
		});

		socket.on('read', function (data) {
			Message.findOne({'documento.room': data.room}, function (err, message) {
				if (err) {
					console.log('erro');
				} else {
					let update = message.documento;
					for (var i = 0; i < update.messages.length; i++) {
						if (update.messages[i]._id == data.id_message){
							update.messages[i].read = true;
						}
					}
					update.save();
				}
			});

		});

		socket.on('typing', function (data) {
			socket.to(data.room).emit('typing', data);
		});

		socket.on('stop typing', function (data) {
			socket.to(data.room).emit('stop typing', data);
		});

		socket.on('disconnect', function () {
			var tmp = []
			for (var i = 0; i < onlines.length; i++) {
				if (onlines[i].indexOf(socket.id) == -1)
					tmp.push(onlines[i]);
			}
			onlines = tmp;
			io.emit('user left', socket.data);
		});

		socket.on('badge', function () {
			io.in('admin').emit('showModal', 'Você ganhou um badge!!!');
			socket.emit('online', onlines);
		});
	});
}