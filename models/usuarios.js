  module.exports = function(app) {
      let mongoose = app.models.factore.mongoose; 
      let connection = app.models.factore.homologacao;

      let schema = mongoose.Schema({ 
        documento : {
            email : String,
            firstName : String,
            lastName : String,
            password : String,
            empresas : [{
                nome : String,
                cargo : String,
                nivelHierarquico : String,
                posicaoTela : String,
                areaAtuacao : String,
                segmentoEconomico : String
            }],
            cursos : [{
                escola : String,
                curso : String,
                anoConclusao : String
            }
            ],
            interesses : [],
            objetivos : [{
                cargo : String,
                areaAtuacao : String,
                segmentoEconomico : String
            }],
            perfil : String,
            userPerfil_id : String,
            tokenAI : String,
            status : String,
            conexoes : []
        }
    },
    {versionKey: false});

      return connection.model('usuarios', schema);
  };



