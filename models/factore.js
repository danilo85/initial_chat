module.exports = function(app) {
	let mongoose = require('mongoose'); 
	mongoose.Promise = global.Promise;  

	let dbOptions = {
		db: { native_parser: true },
		server: {
			auto_reconnect: true,
			poolSize: 5,
			socketOptions: { keepAlive: 1, connectTimeoutMS: 300000, socketTimeoutMS: 300000 }
		}
	};

	let database = {
		mongoose : mongoose,
		chat : mongoose.createConnection("mongodb://localhost/chat", dbOptions),
		homologacao : mongoose.createConnection("mongodb://localhost/homologacao", dbOptions),
	}
	return database;
};