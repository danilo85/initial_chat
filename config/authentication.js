  module.exports = function(app) {
    const crypto = require('crypto');

    let Authentication = {

      encrypt : function (text, password){
        let cipher = crypto.createCipher('aes-256-ctr', password);
        let crypted = cipher.update(text,'utf8','hex');
        crypted += cipher.final('hex');
        return crypted;
      },

      decrypt : function (text, password){
        let decipher = crypto.createDecipher('aes-256-ctr', password);
        let dec = decipher.update(text,'hex','utf8');
        dec += decipher.final('utf8');
        return dec;
      }
    };

    return Authentication;
  };