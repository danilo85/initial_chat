module.exports = function(app) {
  let mongoose = app.models.factore.mongoose;
  let connection = app.models.factore.chat;

  let schema = mongoose.Schema({ 
    documento : {
      room: String,
      messages: [{
        current_time: Date,
        read : Boolean,
        who: {
          user: {
            name: String,
            email: String
          },
          message: String
        }
      }]
    }
  },
  {versionKey: false});
  
  return connection.model('message', schema);
};